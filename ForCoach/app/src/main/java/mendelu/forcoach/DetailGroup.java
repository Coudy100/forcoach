package mendelu.forcoach;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.adapters.AttendantAdapter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroupParticipation;

public class DetailGroup extends AppCompatActivity {
    private RecyclerView recyclerView;
    private AttendantAdapter attendantAdapter;
    private List<Attendant> myGroupAttendantList;
    private List<MyGroupParticipation> myGroupParticipations;

    MyGroup group;

    TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_group);

        int id = getIntent().getIntExtra("id",0);

        group = Setter.getGroupById(id);

        name = (TextView) findViewById(R.id.textView_groupDetailName);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_datailGroup);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        name.setText(group.getName());
        this.setTitle("Group Detail");
        myGroupAttendantList = new ArrayList<>();
        myGroupParticipations = Setter.getParticipationsByGroupId(group.getId());


        for (MyGroupParticipation participation: myGroupParticipations) {
            Attendant attendant = Setter.getAttendantById(participation.getId_Attendant());
            myGroupAttendantList.add(attendant);
        }

        attendantAdapter = new AttendantAdapter(this, myGroupAttendantList);
        recyclerView.setAdapter(attendantAdapter);

    }

}
