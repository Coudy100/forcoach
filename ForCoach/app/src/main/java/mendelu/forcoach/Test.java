package mendelu.forcoach;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mendelu.forcoach.mendelu.forcoach.adapters.GroupAdapterInAttendantDetail;

/**
 * Created by Host on 21.05.2017.
 */

public class Test extends DialogFragment {
    RecyclerView recyclerView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_add_group_in_attendant, container);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView_addGroup_in_Attendant);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        //GroupAdapterInAttendantDetail adapter = new GroupAdapterInAttendantDetail(this.getActivity(), )

        return rootView;
    }
}
