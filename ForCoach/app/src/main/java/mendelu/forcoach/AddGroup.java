package mendelu.forcoach;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.List;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.adapters.AttendantAdapterInAddGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.AddGroupAttendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;

public class AddGroup extends AppCompatActivity {
    RecyclerView recyclerView;
    List<AddGroupAttendant> addGroupAttendants;

    EditText editName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        this.setTitle("Add MyGroup");

        addGroupAttendants = new ArrayList<>();
        for (Attendant attendant: Setter.getAllAttendants()) {
            addGroupAttendants.add(new AddGroupAttendant(attendant));
        }

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_addGroup);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new AttendantAdapterInAddGroup(this, addGroupAttendants));

        editName = (EditText) findViewById(R.id.group_name);


        Button btnAdd = (Button) findViewById(R.id.btn_addGroup);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addGroup();
            }
        });

    }

    private void addGroup() {
        Log.d("HLASKA", "vytvarim MyGroup");
        String name = editName.getText().toString().trim();

        long idGroup = Setter.addGroupSetter(name);

        List<Attendant> participatedAttendants = new ArrayList<>();

        int attendantPosition = 0;
        for (AddGroupAttendant attendant: addGroupAttendants) {
            if (attendant.getParticipation()){
                participatedAttendants.add(Setter.getAllAttendants().get(attendantPosition));
            }
            attendantPosition++;
        }
        for (Attendant attendant: participatedAttendants){
            Setter.addParticipation(idGroup, attendant.getId());
        }



        finish();




    }


}
