package mendelu.forcoach;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.adapters.GroupAdapter;
import mendelu.forcoach.mendelu.forcoach.adapters.GroupAdapterInAttendantDetail;
import mendelu.forcoach.mendelu.forcoach.adapters.LessonAdapter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendance;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Lesson;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroupParticipation;

public class DetailAttendant extends AppCompatActivity {
    private RecyclerView recyclerViewLessons;
    private RecyclerView recyclerViewGroups;
    private LessonAdapter lessonAdapter;
    private GroupAdapter groupAdapter;

    private List<Lesson> lessonList;
    private List<MyGroup> myGroupList;

    private Attendant attendant;

    private TextView name;
    private TextView surname;
    private TextView nickname;
    private TextView phone;
    private TextView email;

    private Button btnAddGroup;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_attendant);
        this.setTitle("Attendant Detail");

        final int id = getIntent().getIntExtra("id", 0);

        attendant = Setter.getAttendantById(id);
        context = this;

        setAtributesToTextViews();
        setGroupsAndLessonsLists();

        lessonAdapter = new LessonAdapter(this, lessonList);
        groupAdapter = new GroupAdapter(this, myGroupList);

        recyclerViewLessons.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewGroups.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewLessons.setAdapter(lessonAdapter);
        recyclerViewGroups.setAdapter(groupAdapter);

        btnAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailAttendant.this, AddGroupParticipationInDetailAttendant.class);
                intent.putExtra("id", attendant.getId());
                startActivity(intent);
            }
        });
    }

    private void setGroupsAndLessonsLists() {
        List<MyGroupParticipation> myGroupParticipationList = Setter.getParticipationsByAttendantId(attendant.getId());
        List<Attendance> attendanceList = Setter.getAttendancesByAttendantId(attendant.getId());

        myGroupList = new ArrayList<>();
        lessonList = new ArrayList<>();
        for (MyGroupParticipation participation : myGroupParticipationList){
            myGroupList.add(Setter.getGroupById(participation.getId_Group()));
        }
        for (Attendance attendance : attendanceList) {
            int id = attendance.getLesson().getId();
            lessonList.add(Setter.getLessonById(id));
        }


    }

    private void setAtributesToTextViews() {
        name = (TextView) findViewById(R.id.textView_attendant_detail_name);
        surname = (TextView) findViewById(R.id.textView_attendant_detail_surname);
        nickname = (TextView) findViewById(R.id.textView_attendant_detail_nickname);
        email = (TextView) findViewById(R.id.textView_attendant_detail_email);
        phone = (TextView) findViewById(R.id.textView_attendant_detail_phone);
        btnAddGroup = (Button) findViewById(R.id.btn_addGroup_attendant_detail);
        recyclerViewGroups = (RecyclerView) findViewById(R.id.recyclerView_groups_attendant_detail);
        recyclerViewLessons = (RecyclerView) findViewById(R.id.recyclerView_lessons_attendant_detail);

        name.setText(attendant.getName());
        surname.setText(attendant.getSurname());
        nickname.setText(attendant.getNickname());
        phone.setText(attendant.getPhone());
        email.setText(attendant.getEmail());
    }
}
