package mendelu.forcoach.mendelu.forcoach.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import mendelu.forcoach.DetailAttendant;
import mendelu.forcoach.R;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;

/**
 * Created by Host on 24.04.2017.
 */

public class AttendantAdapter extends RecyclerView.Adapter<AttendantAdapter.AttendantHolder> {

    private List<Attendant> attendantList;
    private LayoutInflater inflater;
    private Context context;

    public AttendantAdapter(Context context, List<Attendant> attendants) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.attendantList = attendants;
    }

    @Override
    public AttendantHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_attendant, parent, false);
        return new AttendantHolder(view);
    }

    @Override
    public void onBindViewHolder(AttendantHolder holder, final int position) {
        Attendant attendant = attendantList.get(position);
        setNameTextField(holder, attendant);
        holder.textViewPhone.setText("tel. " + attendant.getPhone());

        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), DetailAttendant.class);
                intent.putExtra("id", attendantList.get(position).getId());
                context.startActivity(intent);
            }
        });




    }

    private void setNameTextField(AttendantHolder holder, Attendant attendant) {
        String name = attendant.getName();
        if (attendant.getNickname().length() != 0) {
            name = name + " \"" + attendant.getNickname() + "\" " + attendant.getSurname();
        } else {
            name = name + " " + attendant.getSurname();
        }

        if(name.length() < 27) {
            holder.textViewName.setText(name);
        } else if (name.length() < 40){
            holder.textViewName.setTextSize(18);
            holder.textViewName.setText(name);
        } else {
            holder.textViewName.setTextSize(15);
            holder.textViewName.setText(name);
        }
    }

    @Override
    public int getItemCount() {
        return attendantList.size();
    }

    public class AttendantHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView textViewName;
        private TextView textViewPhone;
        private View row;

        public AttendantHolder(View itemView) {
            super(itemView);

            row = itemView.findViewById(R.id.row_root_attendant);
            textViewName = (TextView) itemView.findViewById(R.id.row_name);
            textViewPhone = (TextView) itemView.findViewById(R.id.row_contact);

            row.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {



        }
    }


}
