package mendelu.forcoach.mendelu.forcoach.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import mendelu.forcoach.R;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.AddLessonGroup;

/**
 * Created by Host on 18.05.2017.
 */

public class GroupAdapterInAddLesson extends RecyclerView.Adapter<GroupAdapterInAddLesson.GroupAddLessonHolder>  {
    private List<AddLessonGroup> groupList;
    private LayoutInflater inflater;

    public GroupAdapterInAddLesson(Context context, List<AddLessonGroup> lessons) {
        this.inflater = LayoutInflater.from(context);
        this.groupList = lessons;
    }

    @Override
    public GroupAddLessonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_add_group_attendant, parent, false);
        return new GroupAddLessonHolder(view);
    }

    @Override
    public void onBindViewHolder(GroupAddLessonHolder holder, int position) {
        final AddLessonGroup group = groupList.get(position);
        holder.textViewName.setText(group.getMyGroup().getName());
        holder.participation.setChecked(false);
        holder.participation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                group.setLessonParticipation(isChecked);
            }
        });

    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }


    protected class GroupAddLessonHolder extends RecyclerView.ViewHolder {
        private TextView textViewName;
        private Switch participation;

        private GroupAddLessonHolder(View itemView) {
            super(itemView);


            textViewName = (TextView) itemView.findViewById(R.id.row_add_group_attendant_name);
            participation = (Switch) itemView.findViewById(R.id.row_add_group_attendant_switch);


        }

    }
}
