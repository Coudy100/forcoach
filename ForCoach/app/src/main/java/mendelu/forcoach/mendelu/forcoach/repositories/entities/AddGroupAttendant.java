package mendelu.forcoach.mendelu.forcoach.repositories.entities;

/**
 * Created by Host on 28.04.2017.
 */

public class AddGroupAttendant {
    private Attendant attendant;
    private Boolean participation;

    public AddGroupAttendant(Attendant attendant) {
        this.attendant = attendant;
        participation = false;
    }

    public Attendant getAttendant() {
        return attendant;
    }

    public void setAttendant(Attendant attendant) {
        this.attendant = attendant;
    }

    public Boolean getParticipation() {
        return participation;
    }

    public void setParticipation(Boolean participation) {
        this.participation = participation;
    }


}
