package mendelu.forcoach.mendelu.forcoach.repositories;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import mendelu.forcoach.mendelu.forcoach.contract.Contract;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroupParticipation;

/**
 * Created by Host on 28.04.2017.
 */

public class RepositoryGroupParticipation {

    public static List<MyGroupParticipation> getParticipationsByGroupId(SQLiteDatabase db, int idGroup) {
        Log.d("HLASKA", "Nacitam participationByGroupId");

        List<MyGroupParticipation> list = new ArrayList<>();
        String WHERE = Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_GROUPID + " = " + Integer.toString(idGroup);

        Cursor cursor = db.query(Contract.FeedAttendantGroup.TABLE_ATTENDANTGROUP, null, WHERE, null, null, null, null);
        while(cursor.moveToNext())      {
            list.add(new MyGroupParticipation(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ATTENDANTID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_GROUPID))));
        }
        cursor.close();
        db.close();
        Log.d("HLASKA", "Nacetl jsem " + Integer.toString(list.size()) + " participationByGroup");


        return list;
    }

    public static List<MyGroupParticipation> getParticipationsByAttendantId(SQLiteDatabase db, int idAttendant) {
        Log.d("HLASKA", "Nacitam participationByAttendantId");

        List<MyGroupParticipation> list = new ArrayList<>();
        String WHERE = Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ATTENDANTID + " = " + Integer.toString(idAttendant);

        Cursor cursor = db.query(Contract.FeedAttendantGroup.TABLE_ATTENDANTGROUP, null, WHERE, null, null, null, null);
        while(cursor.moveToNext())      {
            list.add(new MyGroupParticipation(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ATTENDANTID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_GROUPID))));
        }
        cursor.close();
        db.close();
        Log.d("HLASKA", "Nacetl jsem " + Integer.toString(list.size()) + " participationByAttendantId");


        return list;
    }

    public static MyGroupParticipation getParticipationsByGroupAndAttendantId(SQLiteDatabase db, int idAttendant, int idGroup) {
        Log.d("HLASKA", "Nacitam participationByAttendantId");

        MyGroupParticipation participation = null;
        String WHERE = Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ATTENDANTID + " = " + Integer.toString(idAttendant) + " AND " + Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_GROUPID + " = " + Integer.toString(idGroup);

        Cursor cursor = db.query(Contract.FeedAttendantGroup.TABLE_ATTENDANTGROUP, null, WHERE, null, null, null, null);
        while(cursor.moveToNext())      {
            participation = new MyGroupParticipation(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ATTENDANTID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_GROUPID)));
        }
        cursor.close();
        db.close();
        return participation;
    }

    public static void insertParticipation(SQLiteDatabase db, int id_atttendant, long id_group) {
        Log.d("HLASKA", "pokus o vlozeni Participation");

        ContentValues values = new ContentValues();
        values.put(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ATTENDANTID, id_atttendant);
        values.put(Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_GROUPID, id_group);

        db.insertWithOnConflict(Contract.FeedAttendantGroup.TABLE_ATTENDANTGROUP, null, values, SQLiteDatabase.CONFLICT_REPLACE);

        Log.d("HLASKA", "vlozen Participation");
    }

    public static void deleteParticipationByIdGroup(SQLiteDatabase db, Integer id_group){
        try {
            Log.d("HLASKA", "jsem v daleteParticipationByGroup");

            db.delete(Contract.FeedAttendantGroup.TABLE_ATTENDANTGROUP, Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_GROUPID + " = " + Integer.toString(id_group), null);
            Log.d("HLASKA", "jsem po deleteParticipation");

        } catch (Exception e) {
            Log.d("HLASKA", "delete se nepovedl");
        }

    }

    public static void deleteParticipationByIdAttendant(SQLiteDatabase db, Integer id_attendant) {
        try {
            Log.d("HLASKA", "jsem v daleteParticipationByAttendant");
            db.delete(Contract.FeedAttendantGroup.TABLE_ATTENDANTGROUP, Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ATTENDANTID + " = " + Integer.toString(id_attendant), null);
            Log.d("HLASKA", "jsem po deleteParticipationByAttendant");

        } catch (Exception e) {
            Log.d("HLASKA", "delete se nepovedl");
        }
    }

    public static void deleteParticipationByAttendantAndGroupId(SQLiteDatabase db, int id_group, int id_attendant) {
        try {
            Log.d("HLASKA", "jsem v daleteParticipationByAttendantAndGroup");
            int id = getParticipationsByGroupAndAttendantId(db, id_group, id_attendant).getId();
            String WHERE = Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ID + " = " + Integer.toString(id);
            db.delete(Contract.FeedAttendantGroup.TABLE_ATTENDANTGROUP, WHERE, null);
            Log.d("HLASKA", "jsem po deleteParticipationByAttendantAndGroup");

        } catch (Exception e) {
            Log.d("HLASKA", "delete se nepovedl");
        }
    }
}
