package mendelu.forcoach.mendelu.forcoach.repositories.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Host on 25.04.2017.
 */

public class Lesson {
    private int id;
    private String name;
    private String date;
    private String time;

    public Lesson(int id, String name, String date, String time) {
        setId(id);
        setName(name);
        setDate(date);
        setTime(time);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public Date getDateTimeInDateFormat() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String date = getDate() + " " + getTime();
        Date myDate = dateFormat.parse(date);
        return myDate;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Date getTimeInDateFormat() throws ParseException {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        Date myTime = timeFormat.parse(this.time);

        return myTime;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
