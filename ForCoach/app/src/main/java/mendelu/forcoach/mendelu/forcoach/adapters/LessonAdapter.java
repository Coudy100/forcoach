package mendelu.forcoach.mendelu.forcoach.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import mendelu.forcoach.DetailLesson;
import mendelu.forcoach.R;
import mendelu.forcoach.mendelu.forcoach.repositories.RepositoryLesson;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Lesson;

/**
 * Created by Host on 26.04.2017.
 */

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.LessonHolder> {

    private List<Lesson> lessonList;
    private LayoutInflater inflater;
    private Context context;

    public LessonAdapter(Context context, List<Lesson> lessons) {
        this.inflater = LayoutInflater.from(context);
        this.lessonList = lessons;
        this.context = context;

    }

    @Override
    public LessonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_lesson, parent, false);
        return new LessonAdapter.LessonHolder(view);
    }

    @Override
    public void onBindViewHolder(LessonHolder holder, final int position) {
        Log.d("HLASKA", "jsem v onBindHolderu");
        Lesson lesson = lessonList.get(position);
        holder.textViewName.setText(lesson.getName());
        holder.textViewDateAndTime.setText(lesson.getDate() + " " + lesson.getTime());
        try {
            if (RepositoryLesson.getCurrentDateTime().after(lesson.getDateTimeInDateFormat())) {
                holder.mainColor.setBackgroundColor(Color.GRAY);
            }

            holder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("HLASKA", "kliknul jsi na " + Integer.toString(lessonList.get(position).getId()) );

                    Intent intent = new Intent(v.getContext(), DetailLesson.class);
                    intent.putExtra("id", lessonList.get(position).getId());
                    context.startActivity(intent);

                }
            });
        } catch (ParseException e) {
            Log.d("HLASKA", "Chyba v parsovani data");
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.lessonList.size();
    }


    public class LessonHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView textViewName;
        private TextView textViewDateAndTime;
        private ImageView mainColor;
        private View row;

        public LessonHolder(View itemView) {
            super(itemView);

            row = itemView.findViewById(R.id.row_root_lesson);
            textViewName = (TextView) itemView.findViewById(R.id.row_lesson_name);
            textViewDateAndTime = (TextView) itemView.findViewById(R.id.row_lesson_datetime);
            mainColor = (ImageView) itemView.findViewById(R.id.imageView_lesson_mainColor);

            row.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {



        }
    }
}
