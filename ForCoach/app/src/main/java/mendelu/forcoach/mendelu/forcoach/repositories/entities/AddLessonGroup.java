package mendelu.forcoach.mendelu.forcoach.repositories.entities;

/**
 * Created by Host on 18.05.2017.
 */

public class AddLessonGroup {
    private MyGroup myGroup;
    private Boolean LessonParticipation;

    public AddLessonGroup(MyGroup myGroup, Boolean lessonParticipation) {

        this.myGroup = myGroup;
        LessonParticipation = lessonParticipation;
    }

    public MyGroup getMyGroup() {
        return myGroup;
    }

    public void setMyGroup(MyGroup myGroup) {
        this.myGroup = myGroup;
    }

    public Boolean getLessonParticipation() {
        return LessonParticipation;
    }

    public void setLessonParticipation(Boolean lessonParticipation) {
        LessonParticipation = lessonParticipation;
    }
}
