package mendelu.forcoach.mendelu.forcoach.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import mendelu.forcoach.AddGroup;
import mendelu.forcoach.DetailGroup;
import mendelu.forcoach.MainActivity;
import mendelu.forcoach.R;
import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroupParticipation;

/**
 * Created by Host on 25.04.2017.
 */

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.GroupHolder> {
    private List<MyGroup> groupList;
    private LayoutInflater inflater;
    private Context context;

    public GroupAdapter(Context context, List<MyGroup> groups) {
        this.inflater = LayoutInflater.from(context);
        this.groupList = groups;
        this.context = context;
    }

    @Override
    public GroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.row_group, parent, false);
        return new GroupAdapter.GroupHolder(view);
    }

    @Override
    public void onBindViewHolder(GroupAdapter.GroupHolder holder, final int position) {
        final MyGroup group = groupList.get(position);
        holder.textViewName.setText(group.getName());
        List<MyGroupParticipation> myGroupParticipations = Setter.getParticipationsByGroupId(group.getId());
        holder.textViewGroupSize.setText("Members: " + myGroupParticipations.size());

        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("HLASKA", "kliknul jsi na " + Integer.toString(groupList.get(position).getId()) );

                Intent intent = new Intent(v.getContext(), DetailGroup.class);
                intent.putExtra("id", groupList.get(position).getId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }

    public class GroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView textViewName;
        private TextView textViewGroupSize;

        private View row;

        public GroupHolder(View itemView) {
            super(itemView);

            row = itemView.findViewById(R.id.row_root_group);
            textViewName = (TextView) itemView.findViewById(R.id.row_group_name);
            textViewGroupSize = (TextView) itemView.findViewById(R.id.row_group_size);


            row.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            //Setter.detailGroup(groupList.get(itemView.getId()));
        }
    }
}
