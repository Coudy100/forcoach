package mendelu.forcoach.mendelu.forcoach.repositories;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.DateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.sql.Time;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mendelu.forcoach.mendelu.forcoach.contract.Contract;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Lesson;

/**
 * Created by Host on 25.04.2017.
 */

public class RepositoryLesson {
    public static List<Lesson> getLessons(SQLiteDatabase db){
        List<Lesson> list = new ArrayList<>();

        String ORDER_BY = "date, time ";

        Log.d("HLASKA", "jsem pred nactenim z databaze v getLessons");

        Cursor cursor = db.query(Contract.FeedLessons.TABLE_LESSONS, null, null, null, null, null, ORDER_BY);
        while(cursor.moveToNext()) {
            list.add(new Lesson(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_ID)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_NAME)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_DATE)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_TIME))));
        }
        Log.d("HLASKA", "jsem po nactenim z databaze v getLessons");
        cursor.close();
        db.close();

        return list;
    }

    public static long insertLesson(SQLiteDatabase db, String name, String date, String time){
        Log.d("HLASKA", "jsem v insertLesson");

        ContentValues values = new ContentValues();

        values.put(Contract.FeedLessons.COLUMN_LESSONS_NAME, name);
        values.put(Contract.FeedLessons.COLUMN_LESSONS_DATE, date);
        values.put(Contract.FeedLessons.COLUMN_LESSONS_TIME, time);

        long id = db.insertWithOnConflict(Contract.FeedLessons.TABLE_LESSONS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
        Log.d("HLASKA", "jsem po insertLesson");
        return id;
    }

    public static void deleteLesson(SQLiteDatabase db, Integer id){
        try {
            Log.d("HLASKA", "jsem v deleteLesson");

            db.delete(Contract.FeedLessons.TABLE_LESSONS, Contract.FeedLessons.COLUMN_LESSONS_ID + " = " + Integer.toString(id), null);
            Log.d("HLASKA", "jsem po deleteLesson");

        } catch (Exception e) {
            Log.d("HLASKA", "delete se nepovedl");
        }

    }

    public static Lesson getLessonById(SQLiteDatabase db, int id) {
        String whereClause = Contract.FeedLessons.COLUMN_LESSONS_ID + " = " + Integer.toString(id);
        Cursor cursor = db.query(Contract.FeedLessons.TABLE_LESSONS, null, whereClause, null, null, null, null);
        Lesson lesson = null;

        while(cursor.moveToNext()) {
            lesson = new Lesson(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_ID)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_NAME)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_DATE)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_TIME)));

        }

        return lesson;
    }

    public static Lesson getNearestLesson(SQLiteDatabase db) throws ParseException {
        Date currentDateTime = getCurrentDateTime();
        Cursor cursor = db.query(Contract.FeedLessons.TABLE_LESSONS, null, null, null, null, null, Contract.FeedLessons.COLUMN_LESSONS_DATE + " DESC, " + Contract.FeedLessons.COLUMN_LESSONS_TIME, null );
        Lesson finalLesson = null;



        while(cursor.moveToNext()) {
            Lesson newLesson = new Lesson(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_ID)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_NAME)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_DATE)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedLessons.COLUMN_LESSONS_TIME)));

            if(newLesson.getDateTimeInDateFormat().after(currentDateTime)) {
                if (finalLesson == null) {
                    finalLesson = newLesson;
                }
                if (newLesson.getDateTimeInDateFormat().before(finalLesson.getDateTimeInDateFormat()) && (newLesson.getDateTimeInDateFormat().before(finalLesson.getDateTimeInDateFormat()))) {
                    finalLesson = newLesson;
                }
            }
        }

        return finalLesson;


    }

    public static Date getCurrentDateTime() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Date date = new Date();
        String dateString =  dateFormat.format(date);
        return dateFormat.parse(dateString);

    }



}
