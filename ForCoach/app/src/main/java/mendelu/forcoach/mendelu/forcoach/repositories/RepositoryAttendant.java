package mendelu.forcoach.mendelu.forcoach.repositories;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.contract.Contract;
import mendelu.forcoach.mendelu.forcoach.contract.Contract.FeedAttendants;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroupParticipation;

/**
 * Created by Host on 11.04.2017.
 */

public final class RepositoryAttendant {



    public static List<Attendant> getAttendants(SQLiteDatabase db){
        List<Attendant> list = new ArrayList<>();

        String ORDER_BY = "surname, name";

        Cursor cursor = db.query(FeedAttendants.TABLE_ATTENDANTS, null, null, null, null, null, ORDER_BY);
        while(cursor.moveToNext())      {
            list.add(new Attendant(
                    cursor.getInt(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_ID)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_NAME)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_SURNAME)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_NICKNAME)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_PHONE)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_EMAIL))));
        }
        cursor.close();
        db.close();

        return list;
    }
    public static List<Attendant> getAttendantsByGroupsId(ArrayList<Integer> groupsId){
        List<Attendant> attendantList = new ArrayList<>();
        Set<Integer> attendantSet = new HashSet<Integer>();

        for (Integer idGroup : groupsId) {
            List<MyGroupParticipation> participationList = Setter.getParticipationsByGroupId(idGroup);
            for (MyGroupParticipation participation: participationList) {
                if ( attendantSet.add(participation.getId_Attendant()))
                    attendantList.add(Setter.getAttendantById(participation.getId_Attendant()));
            }
        }
        return attendantList;
    }

    public static Attendant getAttendantById(SQLiteDatabase db, int id) {
        String whereClause = FeedAttendants.COLUMN_ATTENDANTS_ID + " = " + Integer.toString(id);
        Cursor cursor = db.query(FeedAttendants.TABLE_ATTENDANTS, null, whereClause, null, null, null, null);
        Attendant attendant = null;

        while(cursor.moveToNext()) {
            attendant = new Attendant(
                    cursor.getInt(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_ID)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_NAME)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_SURNAME)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_NICKNAME)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_PHONE)),
                    cursor.getString(cursor.getColumnIndex(FeedAttendants.COLUMN_ATTENDANTS_EMAIL)));

        }

        return attendant;
    }
    public static void insertAttendant(SQLiteDatabase db, String name, String surname, String nickname, String phone, String email){
        Log.d("HLASKA", "jsem v insertuAttendanta");

        ContentValues values = new ContentValues();
        values.put(FeedAttendants.COLUMN_ATTENDANTS_NAME, name);
        values.put(FeedAttendants.COLUMN_ATTENDANTS_SURNAME, surname);
        values.put(FeedAttendants.COLUMN_ATTENDANTS_NICKNAME, nickname);
        values.put(FeedAttendants.COLUMN_ATTENDANTS_PHONE, phone);
        values.put(FeedAttendants.COLUMN_ATTENDANTS_EMAIL, email);

        db.insertWithOnConflict(FeedAttendants.TABLE_ATTENDANTS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
        Log.d("HLASKA", "jsem po insertAttendant");
    }
    public static void deleteAttendant(SQLiteDatabase db, Integer id){
        try {
            Log.d("HLASKA", "jsem v deleteAttendant");

            db.delete(FeedAttendants.TABLE_ATTENDANTS, FeedAttendants.COLUMN_ATTENDANTS_ID + " = " + Integer.toString(id), null);
            Log.d("HLASKA", "jsem po deleteAttendant");

        } catch (Exception e) {
            Log.d("HLASKA", "delete se nepovedl");
        }

    }
}
