package mendelu.forcoach.mendelu.forcoach.repositories.entities;

/**
 * Created by Host on 24.04.2017.
 */

public class Attendant {
    private int id;
    private String name;
    private String surname;
    private String nickname;
    private String phone;
    private String email;

    public Attendant(int id, String name, String surname, String nickname, String phone, String email) {
        setId(id);
        setName(name);
        setSurname(surname);
        setNickname(nickname);
        setEmail(email);
        setPhone(phone);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}