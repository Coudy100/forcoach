package mendelu.forcoach.mendelu.forcoach.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import mendelu.forcoach.R;
import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.AddLessonGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroupParticipation;

/**
 * Created by Host on 21.05.2017.
 */

public class GroupAdapterInAttendantDetail extends RecyclerView.Adapter<GroupAdapterInAttendantDetail.GroupInAttendantDetailHolder>  {
    private List<MyGroup> groupList;
    private int idAttendant;
    private LayoutInflater inflater;

    public GroupAdapterInAttendantDetail(Context context, List<MyGroup> groups, int id_attendant) {
        this.inflater = LayoutInflater.from(context);
        this.idAttendant = id_attendant;
        this.groupList = groups;
    }

    @Override
    public GroupAdapterInAttendantDetail.GroupInAttendantDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_add_group_attendant, parent, false);
        return new GroupInAttendantDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(GroupInAttendantDetailHolder holder, int position) {
        final MyGroup group = groupList.get(position);
        holder.textViewName.setText(group.getName());
        if (Setter.getParticipationByGroupAndAttendantId(group.getId(), idAttendant) != null) {
            holder.participation.setChecked(true);
        } else {
            holder.participation.setChecked(false);
        }

        holder.participation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Setter.addParticipation(group.getId(), idAttendant);
                } else {
                    Setter.deleteParticipation(group.getId(), idAttendant);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }


    protected class GroupInAttendantDetailHolder extends RecyclerView.ViewHolder {
        private TextView textViewName;
        private Switch participation;

        private GroupInAttendantDetailHolder(View itemView) {
            super(itemView);


            textViewName = (TextView) itemView.findViewById(R.id.row_add_group_attendant_name);
            participation = (Switch) itemView.findViewById(R.id.row_add_group_attendant_switch);


        }

    }
}
