package mendelu.forcoach.mendelu.forcoach.repositories.entities;

import mendelu.forcoach.mendelu.forcoach.Setter;

/**
 * Created by Host on 18.05.2017.
 */

public class Attendance {
    private int id;
    private Attendant attendant;
    private Lesson lesson;
    private boolean participation;

    public Attendance(int id, int id_Attendant, int id_Lesson, boolean participation) {
        this.id = id;
        this.attendant = Setter.getAttendantById(id_Attendant);
        this.lesson = Setter.getLessonById(id_Lesson);
        this.participation = participation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Attendant getAttendant() {
        return attendant;
    }

    public void setAttendant(Attendant attendant) {
        this.attendant = attendant;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public boolean getParticipation() {
        return participation;
    }

    public void setParticipation(boolean participation) {
        this.participation = participation;
    }
}
