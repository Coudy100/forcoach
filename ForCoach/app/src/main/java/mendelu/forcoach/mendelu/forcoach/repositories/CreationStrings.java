package mendelu.forcoach.mendelu.forcoach.repositories;

import mendelu.forcoach.mendelu.forcoach.contract.Contract;

/**
 * Created by Host on 11.04.2017.
 */

public final class CreationStrings {
    public static String SQL_CREATE_TABLE_ATTENDANTS = "CREATE TABLE " + Contract.FeedAttendants.TABLE_ATTENDANTS + " (" +
            Contract.FeedAttendants.COLUMN_ATTENDANTS_ID + " INTEGER PRIMARY KEY, " +
            Contract.FeedAttendants.COLUMN_ATTENDANTS_NAME + " TEXT, " +
            Contract.FeedAttendants.COLUMN_ATTENDANTS_SURNAME + " TEXT, " +
            Contract.FeedAttendants.COLUMN_ATTENDANTS_NICKNAME + " TEXT, " +
            Contract.FeedAttendants.COLUMN_ATTENDANTS_PHONE + " TEXT, " +
            Contract.FeedAttendants.COLUMN_ATTENDANTS_EMAIL + " TEXT); ";

    public static String SQL_CREATE_TABLE_GROUPS = "CREATE TABLE " + Contract.FeedGroups.TABLE_GROUPS + " (" +
            Contract.FeedGroups.COLUMN_GROUP_ID + " INTEGER PRIMARY KEY, " +
            Contract.FeedGroups.COLUMN_GROUP_NAME + " TEXT); ";

    public static String SQL_CREATE_TABLE_LESSONS = "CREATE TABLE " + Contract.FeedLessons.TABLE_LESSONS + " (" +
            Contract.FeedLessons.COLUMN_LESSONS_ID + " INTEGER PRIMARY KEY, " +
            Contract.FeedLessons.COLUMN_LESSONS_NAME + " TEXT, " +
            Contract.FeedLessons.COLUMN_LESSONS_DATE + " TEXT, " +
            Contract.FeedLessons.COLUMN_LESSONS_TIME + " TEXT); ";

    public static String SQL_CREATE_TABLE_ATTENDANTGROUP = "CREATE TABLE " + Contract.FeedAttendantGroup.TABLE_ATTENDANTGROUP + " (" +
            Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ID + " INTEGER PRIMARY KEY, " +
            Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ATTENDANTID + " INTEGER, " +
            Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_GROUPID + " INTEGER,  " +
            " FOREIGN KEY (" + Contract.FeedAttendantGroup.COLUMN_ATTENDANTGROUP_ATTENDANTID + ") REFERENCES " + Contract.FeedAttendants.TABLE_ATTENDANTS + "(" + Contract.FeedAttendants.COLUMN_ATTENDANTS_ID + "), " +
            " FOREIGN KEY (" +Contract.FeedAttendantGroup. COLUMN_ATTENDANTGROUP_GROUPID + ") REFERENCES " + Contract.FeedGroups.TABLE_GROUPS + "(" + Contract.FeedGroups.COLUMN_GROUP_ID + ") );";

    public static String SQL_CREATE_TABLE_ATTENDANCE = "CREATE TABLE " + Contract.FeedAttendance.TABLE_ATTENDANCE + " (" +
        Contract.FeedAttendance.COLUMN_ATTENDANCE_ID + " INTEGER PRIMARY KEY, " +
        Contract.FeedAttendance.COLUMN_ATTENDANCE_PARTICIPATION + " BOOLEAN, " +
        Contract.FeedAttendance.COLUMN_ATTENDANCE_ATTENDANTID + " INTEGER, " +
        Contract.FeedAttendance.COLUMN_ATTENDANCE_LESSONID + " INTEGER, " +
        " FOREIGN KEY (" + Contract.FeedAttendance.COLUMN_ATTENDANCE_ATTENDANTID + ") REFERENCES " + Contract.FeedAttendants.TABLE_ATTENDANTS + "(" + Contract.FeedAttendants.COLUMN_ATTENDANTS_ID + "), " +
        " FOREIGN KEY (" + Contract.FeedAttendance.COLUMN_ATTENDANCE_LESSONID + ") REFERENCES " + Contract.FeedLessons.TABLE_LESSONS + "(" + Contract.FeedLessons.COLUMN_LESSONS_ID + ") );";

    public static String SQL_DELETE_TABLE_ATTENDANTS = "DROP TABLE IF EXISTS " + Contract.FeedAttendants.TABLE_ATTENDANTS;
    public static String SQL_DELETE_TABLE_LESSONS = "DROP TABLE IF EXISTS " + Contract.FeedLessons.TABLE_LESSONS;
    public static String SQL_DELETE_TABLE_GROUPS = "DROP TABLE IF EXISTS " + Contract.FeedGroups.TABLE_GROUPS;
    public static String SQL_DELETE_TABLE_ATTENDANTGROUP = "DROP TABLE IF EXISTS " + Contract.FeedAttendantGroup.TABLE_ATTENDANTGROUP;
    public static String SQL_DELETE_TABLE_ATTENDANCE = "DROP TABLE IF EXISTS " + Contract.FeedAttendance.TABLE_ATTENDANCE;



}
