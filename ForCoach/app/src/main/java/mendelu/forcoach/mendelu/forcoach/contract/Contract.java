package mendelu.forcoach.mendelu.forcoach.contract;

import android.provider.BaseColumns;

/**
 * Created by Host on 11.04.2017.
 */

public final class Contract {
    private Contract() {}

    public static class FeedAttendants implements BaseColumns {
        public static final String TABLE_ATTENDANTS = "attendants";
        public static final String COLUMN_ATTENDANTS_ID = "id_attendant";
        public static final String COLUMN_ATTENDANTS_NAME = "name";
        public static final String COLUMN_ATTENDANTS_SURNAME = "surname";
        public static final String COLUMN_ATTENDANTS_NICKNAME = "nickname";
        public static final String COLUMN_ATTENDANTS_PHONE = "phone";
        public static final String COLUMN_ATTENDANTS_EMAIL = "email";
    }

    public static class FeedLessons implements BaseColumns {
        public static final String TABLE_LESSONS = "lessons";
        public static final String COLUMN_LESSONS_ID = "id_lesson";
        public static final String COLUMN_LESSONS_NAME = "name";
        public static final String COLUMN_LESSONS_TIME = "time";
        public static final String COLUMN_LESSONS_DATE = "date";

    }

    public static class FeedGroups implements BaseColumns {
        public static final String TABLE_GROUPS = "groups";
        public static final String COLUMN_GROUP_ID = "id_group";
        public static final String COLUMN_GROUP_NAME = "name";
    }

    public static class FeedAttendance implements BaseColumns {
        public static final String TABLE_ATTENDANCE = "attendance";
        public static final String COLUMN_ATTENDANCE_ID = "id_attendance";
        public static final String COLUMN_ATTENDANCE_PARTICIPATION = "participation";
        public static final String COLUMN_ATTENDANCE_ATTENDANTID = "id_attendant";
        public static final String COLUMN_ATTENDANCE_LESSONID = "id_lesson";
    }

    public static class FeedAttendantGroup implements BaseColumns {
        public static final String TABLE_ATTENDANTGROUP = "attendantGroup";
        public static final String COLUMN_ATTENDANTGROUP_ID = "id";
        public static final String COLUMN_ATTENDANTGROUP_ATTENDANTID = "id_attendat";
        public static final String COLUMN_ATTENDANTGROUP_GROUPID = "id_group";
    }
}
