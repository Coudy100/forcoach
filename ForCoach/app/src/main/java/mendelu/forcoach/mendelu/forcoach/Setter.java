package mendelu.forcoach.mendelu.forcoach;

import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import mendelu.forcoach.DbHandler;
import mendelu.forcoach.MainActivity;
import mendelu.forcoach.mendelu.forcoach.repositories.RepositoryAttendance;
import mendelu.forcoach.mendelu.forcoach.repositories.RepositoryAttendant;
import mendelu.forcoach.mendelu.forcoach.repositories.RepositoryGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.RepositoryLesson;
import mendelu.forcoach.mendelu.forcoach.repositories.RepositoryGroupParticipation;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendance;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Lesson;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroupParticipation;

/**
 * Created by Host on 11.04.2017.
 */

public final class Setter {
    private static DbHandler dbHandler;



    public static void setDbHandler(MainActivity ma) {
        Setter.dbHandler = new DbHandler(ma);
    }


    public static long addLessonSetter(String name, String date, String time) {
        return RepositoryLesson.insertLesson(dbHandler.getWritableDatabase(), name, date, time);
    }
    public static void addAttendantSetter(String name, String surname, String nickname, String phone, String email) {
        RepositoryAttendant.insertAttendant(dbHandler.getWritableDatabase(), name, surname, nickname, phone, email);

    }
    public static long addGroupSetter(String name){
        return RepositoryGroup.insertGroup(dbHandler.getWritableDatabase(), name);

    }
    public static void addParticipation(long idGroup, int idAttendant) {
        RepositoryGroupParticipation.insertParticipation(dbHandler.getWritableDatabase(), idAttendant, idGroup);
    }
    public static void addAttendancesByGroupsId(long idLesson, List<MyGroup> participatedGroups) {
        ArrayList<Integer> groupsId = new ArrayList<>();
        for (MyGroup group : participatedGroups) {

            groupsId.add(group.getId());
        }

        List<Attendant> attendantList = Setter.getAttendantsByGroupsId(groupsId);
        for (Attendant attendant: attendantList) {
            RepositoryAttendance.insertAttendance(dbHandler.getWritableDatabase(), idLesson, attendant.getId(), false);
        }

    }

    public static void deleteLessonSetter(Integer id) {
        RepositoryAttendance.deleteAttendanceByLessonId(dbHandler.getWritableDatabase(), id);
        RepositoryLesson.deleteLesson(dbHandler.getWritableDatabase(), id);
    }
    public static void deleteAttendatntSetter(Integer id){
        RepositoryGroupParticipation.deleteParticipationByIdAttendant(dbHandler.getWritableDatabase(), id);
        RepositoryAttendance.deleteAttendanceByAttendantId(dbHandler.getWritableDatabase(), id);
        RepositoryAttendant.deleteAttendant(dbHandler.getWritableDatabase(), id);
    }
    public static void deleteGroupSetter(Integer id){
        RepositoryGroupParticipation.deleteParticipationByIdGroup(dbHandler.getWritableDatabase(), id);
        RepositoryGroup.deleteGroup(dbHandler.getWritableDatabase(), id);
    }
    public static void deleteParticipation(int id_group, int id_attendant) {
        RepositoryGroupParticipation.deleteParticipationByAttendantAndGroupId(dbHandler.getWritableDatabase(), id_group, id_attendant);
    }
    public static Attendant getAttendantById(int id_attendant) {
        return RepositoryAttendant.getAttendantById(dbHandler.getReadableDatabase(), id_attendant);
    }
    public static List<Attendant> getAttendantsByGroupsId(ArrayList<Integer> groupsId){
        return RepositoryAttendant.getAttendantsByGroupsId(groupsId);
    }
    public static MyGroup getGroupById(int id) {
        return RepositoryGroup.getGroupById(dbHandler.getReadableDatabase(), id);
    }
    public static Lesson getLessonById(int id) {
        return RepositoryLesson.getLessonById(dbHandler.getReadableDatabase(), id);
    }

    public static Lesson getNearestLesson() throws ParseException {
        return RepositoryLesson.getNearestLesson(dbHandler.getReadableDatabase());
    }
    public static List<Attendant> getAllAttendants() {
        return  RepositoryAttendant.getAttendants(dbHandler.getReadableDatabase());

    }
    public static List<Lesson> getAllLessons() {
        List<Lesson> lessonList = RepositoryLesson.getLessons(dbHandler.getReadableDatabase());
        return lessonList;
    }

    public static List<MyGroup> getAllGroups() {
        return RepositoryGroup.getGroups(dbHandler.getReadableDatabase());
    }
    public static List<MyGroupParticipation> getParticipationsByGroupId(int id) {
        return RepositoryGroupParticipation.getParticipationsByGroupId(dbHandler.getReadableDatabase(), id);
    }

    public static List<MyGroupParticipation> getParticipationsByAttendantId(int id) {
        return RepositoryGroupParticipation.getParticipationsByAttendantId(dbHandler.getReadableDatabase(), id);
    }
    public static List<Attendance> getAttendancesByLessonId(int id) {
        return RepositoryAttendance.getAttendancesByLessonId(dbHandler.getReadableDatabase(), id);
    }

    public static List<Attendance> getAttendancesByAttendantId(int id) {
        return RepositoryAttendance.getAttendancesByAttendantId(dbHandler.getReadableDatabase(), id);
    }

    public static void updateAttendance(int attendanceId, boolean participation) {
        RepositoryAttendance.updateAttendance(dbHandler.getWritableDatabase(), attendanceId, participation);
    }

    public static MyGroupParticipation getParticipationByGroupAndAttendantId(int idGroup, int idAttendant) {
        return RepositoryGroupParticipation.getParticipationsByGroupAndAttendantId(dbHandler.getReadableDatabase(), idGroup, idAttendant);
    }
}
