package mendelu.forcoach.mendelu.forcoach.repositories;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.List;

import mendelu.forcoach.mendelu.forcoach.contract.Contract;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroup;


/**
 * Created by Host on 25.04.2017.
 */

public class RepositoryGroup {
    public static List<MyGroup> getGroups(SQLiteDatabase db){
        List<MyGroup> list = new ArrayList<>();

        String[] columns = {
                Contract.FeedGroups.COLUMN_GROUP_ID,
                Contract.FeedGroups.COLUMN_GROUP_NAME,

        };
        String ORDER_BY = "name";

        Log.d("HLASKA", "jsem pred nactenim z databaze v getGroup");

        Cursor cursor = db.query(Contract.FeedGroups.TABLE_GROUPS, columns, null, null, null, null, null);
        Log.d("HLASKA", "cursor v get Groups je vytvoren");
        while(cursor.moveToNext()) {
            list.add(new MyGroup(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedGroups.COLUMN_GROUP_ID)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedGroups.COLUMN_GROUP_NAME))));
        }
        Log.d("HLASKA", "jsem po nactenim z databaze v getGroup");
        cursor.close();
        db.close();

        return list;
    }

    public static long insertGroup(SQLiteDatabase db, String name){
        Log.d("HLASKA", "jsem v insertGroup");

        ContentValues values = new ContentValues();

        values.put(Contract.FeedGroups.COLUMN_GROUP_NAME, name);
        long groupID = db.insertWithOnConflict(Contract.FeedGroups.TABLE_GROUPS, null, values, SQLiteDatabase.CONFLICT_REPLACE);

        db.close();
        Log.d("HLASKA", "jsem po insertGroup");

        return groupID;
    }

    public static void deleteGroup(SQLiteDatabase db, Integer id){
        try {
            Log.d("HLASKA", "jsem v deleteGroup");

            db.delete(Contract.FeedGroups.TABLE_GROUPS, Contract.FeedGroups.COLUMN_GROUP_ID + " = " + Integer.toString(id), null);
            Log.d("HLASKA", "jsem po deleteDeleteGroup");

        } catch (Exception e) {
            Log.d("HLASKA", "delete se nepovedl");
        }

    }

    public static MyGroup getGroupById(SQLiteDatabase db, int id) {
        String whereClause = Contract.FeedGroups.COLUMN_GROUP_ID + " = " + Integer.toString(id);
        Cursor cursor = db.query(Contract.FeedGroups.TABLE_GROUPS, null, whereClause, null, null, null, null);
        MyGroup group = null;

        while(cursor.moveToNext()) {
            group = new MyGroup(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedGroups.COLUMN_GROUP_ID)),
                    cursor.getString(cursor.getColumnIndex(Contract.FeedGroups.COLUMN_GROUP_NAME)));

        }

        return group;
    }
}



