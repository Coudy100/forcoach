package mendelu.forcoach.mendelu.forcoach.repositories;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import mendelu.forcoach.mendelu.forcoach.contract.Contract;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendance;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;


/**
 * Created by Host on 18.05.2017.
 */

public class RepositoryAttendance {
    public static List<Attendance> getAttendancesByLessonId(SQLiteDatabase db, int idLesson) {
        Log.d("HLASKA", "Nacitam attendanceByLessonId");

        List<Attendance> list = new ArrayList<>();
        String WHERE = Contract.FeedAttendance.COLUMN_ATTENDANCE_LESSONID + " = " + Integer.toString(idLesson);

        Cursor cursor = db.query(Contract.FeedAttendance.TABLE_ATTENDANCE, null, WHERE, null, null, null, null);

        while(cursor.moveToNext())      {
            list.add(new Attendance(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_ID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_ATTENDANTID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_LESSONID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_PARTICIPATION)) != 0));
        }
        cursor.close();
        db.close();
        Log.d("HLASKA", "Nacetl jsem " + Integer.toString(list.size()) + " attendancesByLessonId");
        return list;
    }

    public static List<Attendance> getAttendancesByAttendantId(SQLiteDatabase db, int idAttendant) {
        Log.d("HLASKA", "Nacitam attendanceByAttendantId");

        List<Attendance> list = new ArrayList<>();
        String WHERE = Contract.FeedAttendance.COLUMN_ATTENDANCE_ATTENDANTID + " = " + Integer.toString(idAttendant);

        Cursor cursor = db.query(Contract.FeedAttendance.TABLE_ATTENDANCE, null, WHERE, null, null, null, null);

        while(cursor.moveToNext())      {
            list.add(new Attendance(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_ID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_ATTENDANTID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_LESSONID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_PARTICIPATION)) != 0));
        }
        cursor.close();
        db.close();
        Log.d("HLASKA", "Nacetl jsem " + Integer.toString(list.size()) + " attendancesByAttendantId");
        return list;
    }

    public static void insertAttendance(SQLiteDatabase db, long id_lesson, int id_atttendant, boolean participation) {
        Log.d("HLASKA", "pokus o vlozeni Attendance");

        ContentValues values = new ContentValues();
        values.put(Contract.FeedAttendance.COLUMN_ATTENDANCE_ATTENDANTID, id_atttendant);
        values.put(Contract.FeedAttendance.COLUMN_ATTENDANCE_LESSONID, id_lesson);
        values.put(Contract.FeedAttendance.COLUMN_ATTENDANCE_PARTICIPATION, participation);

        db.insertWithOnConflict(Contract.FeedAttendance.TABLE_ATTENDANCE, null, values, SQLiteDatabase.CONFLICT_REPLACE);

        Log.d("HLASKA", "vlozen Participation");
    }

    public static Attendance getAttendanceById(SQLiteDatabase db, int id) {
        String whereClause = Contract.FeedAttendance.COLUMN_ATTENDANCE_ID + " = " + Integer.toString(id);
        Cursor cursor = db.query(Contract.FeedAttendance.TABLE_ATTENDANCE, null, whereClause, null, null, null, null);
        Attendance attendance = null;

        while(cursor.moveToNext()) {
            attendance = new Attendance(
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_ID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_ATTENDANTID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_LESSONID)),
                    cursor.getInt(cursor.getColumnIndex(Contract.FeedAttendance.COLUMN_ATTENDANCE_PARTICIPATION)) != 0);

        }

        return attendance;
    }

    public static void deleteAttendanceByLessonId(SQLiteDatabase db, Integer id_lesson){
        try {
            db.delete(Contract.FeedAttendance.TABLE_ATTENDANCE, Contract.FeedAttendance.COLUMN_ATTENDANCE_LESSONID + " = " + Integer.toString(id_lesson), null);
            Log.d("HLASKA", "vymazal jsem Attendance by LessonId");

        } catch (Exception e) {
            Log.d("HLASKA", "delete Attendance se nepovedl");
        }

    }

    public static void deleteAttendanceByAttendantId(SQLiteDatabase db, Integer id_attendant) {
        try {

            db.delete(Contract.FeedAttendance.TABLE_ATTENDANCE, Contract.FeedAttendance.COLUMN_ATTENDANCE_ATTENDANTID + " = " + Integer.toString(id_attendant), null);
            Log.d("HLASKA", "vymazal jsem Attendance by AttendantId");

        } catch (Exception e) {
            Log.d("HLASKA", "delete Attendance se nepovedl");
        }
    }

    public static void updateAttendance(SQLiteDatabase db, int attendanceId, boolean participation) {
        ContentValues values = new ContentValues();
        values.put(Contract.FeedAttendance.COLUMN_ATTENDANCE_PARTICIPATION, participation);

        String WHERE = Contract.FeedAttendance.COLUMN_ATTENDANCE_ID + " = " + Integer.toString(attendanceId);
        db.update(Contract.FeedAttendance.TABLE_ATTENDANCE, values, WHERE, null);

    }
}
