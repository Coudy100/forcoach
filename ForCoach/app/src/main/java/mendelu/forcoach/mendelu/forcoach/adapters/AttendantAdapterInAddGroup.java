package mendelu.forcoach.mendelu.forcoach.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import mendelu.forcoach.R;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.AddGroupAttendant;

/**
 * Created by Host on 28.04.2017.
 */

public class AttendantAdapterInAddGroup extends RecyclerView.Adapter<AttendantAdapterInAddGroup.AttendantAddGroupHolder> {

    private List<AddGroupAttendant> attendantList;
    private LayoutInflater inflater;

    public AttendantAdapterInAddGroup(Context context, List<AddGroupAttendant> attendants) {
        this.inflater = LayoutInflater.from(context);
        this.attendantList = attendants;
    }

    @Override
    public AttendantAdapterInAddGroup.AttendantAddGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_add_group_attendant, parent, false);
        return new AttendantAddGroupHolder(view);
    }

    @Override
    public void onBindViewHolder(AttendantAddGroupHolder holder, int position) {
        final AddGroupAttendant attendant = attendantList.get(position);
        holder.textViewName.setText(attendant.getAttendant().getName() + " " + attendant.getAttendant().getSurname());
        holder.participation.setChecked(false);
        holder.participation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                attendant.setParticipation(isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return attendantList.size();
    }


    protected class AttendantAddGroupHolder extends RecyclerView.ViewHolder {
        private TextView textViewName;
        private Switch participation;

        private AttendantAddGroupHolder(View itemView) {
            super(itemView);


            textViewName = (TextView) itemView.findViewById(R.id.row_add_group_attendant_name);
            participation = (Switch) itemView.findViewById(R.id.row_add_group_attendant_switch);


        }

    }


}
