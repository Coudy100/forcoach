package mendelu.forcoach.mendelu.forcoach.repositories.entities;

/**
 * Created by Host on 28.04.2017.
 */

public class MyGroup {
    private int id;
    private String name;

    public MyGroup(int id, String name) {

        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
