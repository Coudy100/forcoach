package mendelu.forcoach.mendelu.forcoach.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import mendelu.forcoach.R;
import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendance;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;

/**
 * Created by Host on 18.05.2017.
 */

public class AttendantAdapterInDetailLesson extends RecyclerView.Adapter<AttendantAdapterInDetailLesson.AttendantInDetailLessonHolder> {

    private List<Attendance> attendanceList;
    private LayoutInflater inflater;

    public AttendantAdapterInDetailLesson(Context context, List<Attendance> attendances) {
        this.inflater = LayoutInflater.from(context);
        this.attendanceList = attendances;
    }

    @Override
    public AttendantAdapterInDetailLesson.AttendantInDetailLessonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_add_group_attendant, parent, false);
        return new AttendantInDetailLessonHolder(view);
    }

    @Override
    public void onBindViewHolder(AttendantInDetailLessonHolder holder, int position) {
        final Attendant attendant = attendanceList.get(position).getAttendant();
        final Attendance attendance = attendanceList.get(position);
        holder.textViewName.setText(attendant.getName() + " " + attendant.getSurname());
        holder.attandanceSwitch.setChecked(attendance.getParticipation());
        holder.attandanceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                attendance.setParticipation(isChecked);
                Setter.updateAttendance(attendance.getId(), attendance.getParticipation());
            }

        });
    }

    @Override
    public int getItemCount() {
        return attendanceList.size();
    }


    protected class AttendantInDetailLessonHolder extends RecyclerView.ViewHolder {
        private TextView textViewName;
        private Switch attandanceSwitch;

        private AttendantInDetailLessonHolder(View itemView) {
            super(itemView);


            textViewName = (TextView) itemView.findViewById(R.id.row_add_group_attendant_name);
            attandanceSwitch = (Switch) itemView.findViewById(R.id.row_add_group_attendant_switch);


        }

    }


}
