package mendelu.forcoach.mendelu.forcoach.repositories.entities;

import mendelu.forcoach.mendelu.forcoach.Setter;

/**
 * Created by Host on 28.04.2017.
 */

public class MyGroupParticipation {
    private int id;
    private Attendant attendant;
    private MyGroup myGroup;

    public MyGroupParticipation(int id, int id_Attendant, int id_Group) {
        setId(id);
        this.attendant = Setter.getAttendantById(id_Attendant);
        this.myGroup = Setter.getGroupById(id_Group);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Attendant getAttendant() {
        return attendant;
    }

    public MyGroup getMyGroup() {
        return myGroup;
    }

    public int getId_Attendant() {
        return attendant.getId();
    }

    public int getId_Group() {
        return myGroup.getId();
    }


}
