package mendelu.forcoach;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Set;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.adapters.GroupAdapterInAttendantDetail;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;

public class AddGroupParticipationInDetailAttendant extends AppCompatActivity {
    RecyclerView recyclerView;
    TextView name;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_group_in_attendant);

        int id = getIntent().getIntExtra("id", 0);
        final Attendant attendant = Setter.getAttendantById(id);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_addGroup_in_Attendant);
        btnSave = (Button) findViewById(R.id.btn_save_in_Attendant);

        name = (TextView) findViewById(R.id.textView_add_group_in_attendant_name);
        name.setText("Put " + attendant.getName() + " to Groups:");

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddGroupParticipationInDetailAttendant.this, DetailAttendant.class);
                intent.putExtra("id", attendant.getId());
                startActivity(intent);
            }
        });

        GroupAdapterInAttendantDetail adapter = new GroupAdapterInAttendantDetail(this, Setter.getAllGroups(), id);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


    }
}
