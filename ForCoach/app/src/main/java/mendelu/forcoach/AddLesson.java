package mendelu.forcoach;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.adapters.GroupAdapterInAddLesson;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.AddLessonGroup;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroup;

public class AddLesson extends AppCompatActivity {
    Button btn_time;
    static final int DIALOG_TIME = 0;
    Button btn_date;
    static final int DIALOG_DATE = 1;

    int v_hour;
    int v_minute;
    int v_year;
    int v_month;
    int v_day;

    RecyclerView recyclerView;
    List<AddLessonGroup> addLessonGroup;

    EditText editName;
    TextView editTime;
    TextView editDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lesson);
        this.setTitle("Add Lesson");

        addLessonGroup = new ArrayList<>();
        for (MyGroup group: Setter.getAllGroups()) {
            addLessonGroup.add(new AddLessonGroup(group, false));
        }

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_addLesson);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new GroupAdapterInAddLesson(this, addLessonGroup));

        editName = (EditText) findViewById(R.id.group_name);



        Button btnAdd = (Button) findViewById(R.id.btn_addLesson);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                try {
                    addLesson();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        editName = (EditText) findViewById(R.id.lesson_name);
        editDate = (TextView) findViewById(R.id.lesson_date);
        editTime = (TextView) findViewById(R.id.lesson_time);

        showTimePicker();
        showDatePicker();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void addLesson() throws ParseException {
        Log.d("HLASKA", "vytvarim Lesson");
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        DateFormat timeFormat = new SimpleDateFormat("HH:mm");
        Date dateObject;
        Date timeObject;
        String date_v = editDate.getText().toString();
        String time_v = editTime.getText().toString();
        dateObject = dateFormat.parse(date_v);
        timeObject = timeFormat.parse(time_v);


        String date = dateFormat.format(dateObject);
        String time = timeFormat.format(timeObject);
        String name = editName.getText().toString().trim();

        Log.d("HLASKA", date + time);
        long idLesson = Setter.addLessonSetter(name, date, time);

        List<MyGroup> participatedGroups = new ArrayList<>();
        int groupPosition = 0;
        for (AddLessonGroup group: addLessonGroup) {
            if (group.getLessonParticipation()) {
                participatedGroups.add(Setter.getAllGroups().get(groupPosition));
            }
            groupPosition++;
        }

        Setter.addAttendancesByGroupsId(idLesson, participatedGroups);

        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    protected Dialog onCreateDialog(int id) {
        if(id == DIALOG_TIME) {

            return  new TimePickerDialog(this, timePickerListener, v_hour, v_minute, false);

        } else if(id == DIALOG_DATE) {
            DatePickerDialog datePicker = new DatePickerDialog(this, datePickerListener, v_year, v_month, v_day);
            Calendar c = Calendar.getInstance();
            datePicker.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

            return datePicker;
        }
        return null;
    }



    private void showTimePicker() {
        btn_time = (Button) findViewById(R.id.btn_setTimeLesson);
        btn_time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog(DIALOG_TIME);
            }
        });
    }
    private void showDatePicker() {
        btn_date = (Button) findViewById(R.id.btn_setDateLesson);
        btn_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog(DIALOG_DATE);
            }
        });

    }
    protected TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            v_hour = hourOfDay;
            v_minute = minute;

            editTime.setText(v_hour + ":" + v_minute);
        }
    };
    protected  DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            v_year = year;
            v_month = month +1;
            v_day = dayOfMonth;

            editDate.setText(v_day + "." + v_month + "." + v_year );

        }
    };
}
