package mendelu.forcoach;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.io.Console;
import java.util.ArrayList;

import mendelu.forcoach.mendelu.forcoach.repositories.CreationStrings;

/**
 * Created by Host on 07.04.2017.
 */


public class DbHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "database.db";




    public DbHandler(Context context) { //String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CreationStrings.SQL_CREATE_TABLE_ATTENDANTS);
        db.execSQL(CreationStrings.SQL_CREATE_TABLE_GROUPS);
        db.execSQL(CreationStrings.SQL_CREATE_TABLE_LESSONS);
        db.execSQL(CreationStrings.SQL_CREATE_TABLE_ATTENDANTGROUP);
        db.execSQL(CreationStrings.SQL_CREATE_TABLE_ATTENDANCE);
        Log.d("HLASKA", "vytvoril jsem databazi");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(CreationStrings.SQL_DELETE_TABLE_ATTENDANTS);
        db.execSQL(CreationStrings.SQL_DELETE_TABLE_LESSONS);
        db.execSQL(CreationStrings.SQL_DELETE_TABLE_GROUPS);
        db.execSQL(CreationStrings.SQL_DELETE_TABLE_ATTENDANTGROUP);
        db.execSQL(CreationStrings.SQL_DELETE_TABLE_ATTENDANCE);

        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }
}
