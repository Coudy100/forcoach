package mendelu.forcoach;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.text.ParseException;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Lesson;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    protected NavigationView navigationView;
    protected DrawerLayout drawer;
    protected FrameLayout frameLayout;
    protected ActionBarDrawerToggle toggle;

    protected TextView lessonName;
    protected TextView lessonDate;
    protected TextView lessonTime;
    protected TextView textViewNextTraining;
    protected Button btnUpconmmingLessonDetail;

    private Lesson upcommingLesson;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //deleteDatabase("database.db");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        Setter.setDbHandler(this);
        try {
            setUpcommingLesson();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setUpcommingLesson() throws ParseException {
        frameLayout = (FrameLayout) findViewById(R.id.container);
        lessonName = (TextView) findViewById(R.id.textView_mainLessonName);
        lessonDate = (TextView) findViewById(R.id.textView_mainLessonDate);
        lessonTime = (TextView) findViewById(R.id.textView_mainLessonTime);
        textViewNextTraining = (TextView) findViewById(R.id.textView_mainLessonNextTraining);
        btnUpconmmingLessonDetail= (Button) findViewById(R.id.btn_UpcommingLessonDetail);

        upcommingLesson = Setter.getNearestLesson();

        if (upcommingLesson != null) {
            lessonName.setText(upcommingLesson.getName());
            lessonTime.setText(upcommingLesson.getTime());
            lessonDate.setText(upcommingLesson.getDate());


            btnUpconmmingLessonDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayUpcommingLessonDetail();
                }
            });
        } else {
            lessonName.setTextSize(25);
            lessonName.setText(R.string.no_upcomming_lessons);
            lessonTime.setVisibility(View.INVISIBLE);
            lessonDate.setVisibility(View.INVISIBLE);
            btnUpconmmingLessonDetail.setVisibility(View.GONE);
        }
    }
    private void displayUpcommingLessonDetail() {
        Intent intent = new Intent(this, DetailLesson.class);
        intent.putExtra("id", upcommingLesson.getId());
        startActivity(intent);
    }
    protected void setMainInvisible() {
        lessonName.setVisibility(View.INVISIBLE);
        lessonDate.setVisibility(View.INVISIBLE);
        lessonTime.setVisibility(View.INVISIBLE);
        textViewNextTraining.setVisibility(View.INVISIBLE);
        btnUpconmmingLessonDetail.setVisibility(View.GONE);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        toggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == R.id.nav_home) {
            if (!navigationView.getMenu().getItem(0).isChecked()){
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }

        }

        if (id == R.id.nav_attendants) {
            if (!navigationView.getMenu().getItem(1).isChecked()) {
                Intent intent = new Intent(this, ActivityAttendant.class);
                startActivity(intent);
            }

        } else if (id == R.id.nav_lessons) {
            if (!navigationView.getMenu().getItem(2).isChecked()) {
                Intent intent = new Intent(this, ActivityLesson.class);
                startActivity(intent);
            }


        } else if (id == R.id.nav_groups) {
            if (!navigationView.getMenu().getItem(3).isChecked()) {
                Intent intent = new Intent(this, ActivityGroup.class);
                startActivity(intent);
            }


        } else if (id == R.id.nav_manage) {


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == 1) && (resultCode == RESULT_OK)) {

        }
    }
}
