package mendelu.forcoach;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import java.util.List;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.adapters.GroupAdapter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.MyGroup;

public class ActivityGroup extends MainActivity {
    private RecyclerView recyclerView;
    private GroupAdapter groupAdapter;
    private List<MyGroup> groupList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLayoutInflater().inflate(R.layout.activity_group, frameLayout);
        navigationView.getMenu().getItem(3).setChecked(true);
        setMainInvisible();

        this.setTitle("Groups");


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_listGroups);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCllback());
        itemTouchHelper.attachToRecyclerView(recyclerView);

        onResume();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addGroup();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        groupList = Setter.getAllGroups();
        groupAdapter = new GroupAdapter(this, groupList);
        recyclerView.setAdapter(groupAdapter);

    }

    private void addGroup() {
        Intent intent = new Intent(this, AddGroup.class);
        startActivity(intent);

    }
    private ItemTouchHelper.Callback createHelperCllback() {
        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                        int adapterPosition = viewHolder.getAdapterPosition();
                        createDeleteWarning(adapterPosition);


                    }
                };
        return simpleCallback;
    }
    private void createDeleteWarning(final int adapterPosition) {
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
        alertDlg.setMessage("Do you want to delete? ");
        alertDlg.setCancelable(false);

        alertDlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteItem(adapterPosition);
            }
        });

        alertDlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                recyclerView.getAdapter().notifyDataSetChanged();

            }
        });

        alertDlg.create().show();
    }
    private void deleteItem(int adapterPosition) {
        MyGroup group = groupList.get(adapterPosition);
        int id_group = group.getId();
        Setter.deleteGroupSetter(id_group);
        Log.d("HLASKA", "jsem smazani z database");
        groupList.remove(adapterPosition);
        groupAdapter.notifyItemRemoved(adapterPosition);
        onResume();
    }
}
