package mendelu.forcoach;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import java.util.List;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.adapters.AttendantAdapter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;

public class ActivityAttendant extends MainActivity {
    private RecyclerView recyclerView;
    private AttendantAdapter attendantAdapter;
    private List<Attendant> attendantList;

    @Override
    protected void onResume() {
        super.onResume();

        attendantList = Setter.getAllAttendants();
        attendantAdapter = new AttendantAdapter(this, attendantList);
        recyclerView.setAdapter(attendantAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLayoutInflater().inflate(R.layout.activity_attendant, frameLayout);
        navigationView.getMenu().getItem(1).setChecked(true);

        setMainInvisible();

        this.setTitle("Attendants");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_listAttendant);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCllback());
        itemTouchHelper.attachToRecyclerView(recyclerView);

        attendantList = Setter.getAllAttendants();

        attendantAdapter = new AttendantAdapter(this, attendantList);
        recyclerView.setAdapter(attendantAdapter);
        Log.d("HLASKA", "jsem po setPlayerAdapteru");



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAttendant();
            }
        });
    }

    private void addAttendant() {
        Intent intent = new Intent(this, AddAttendant.class);
        startActivity(intent);

    }

    private ItemTouchHelper.Callback createHelperCllback() {
        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                        int adapterPosition = viewHolder.getAdapterPosition();
                        createDeleteWarning(adapterPosition);
                    }
                };
        return simpleCallback;
    }
    private void createDeleteWarning(final int adapterPosition) {
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
        alertDlg.setMessage("Do you want to delete? ");
        alertDlg.setCancelable(false);

        alertDlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteItem(adapterPosition);

            }
        });

        alertDlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                recyclerView.getAdapter().notifyDataSetChanged();

            }
        });

        alertDlg.create().show();
    }
    private void deleteItem(int adapterPosition) {
        Attendant attendant = attendantList.get(adapterPosition);
        Setter.deleteAttendatntSetter(attendant.getId());
        attendantList.remove(adapterPosition);
        attendantAdapter.notifyItemRemoved(adapterPosition);
        onResume();

    }
}
