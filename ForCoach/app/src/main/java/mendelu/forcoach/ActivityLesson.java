package mendelu.forcoach;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import java.util.List;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.adapters.LessonAdapter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Lesson;

public class ActivityLesson extends MainActivity {
    private RecyclerView recyclerView;
    private LessonAdapter lessonAdapter;
    private List<Lesson> lessonList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLayoutInflater().inflate(R.layout.activity_lesson, frameLayout);
        navigationView.getMenu().getItem(2).setChecked(true);
        setMainInvisible();

        this.setTitle("Lessons");
        Log.d("HLASKA", "jsem po setPlayerAdapteru");
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_listLesson);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCllback());
        itemTouchHelper.attachToRecyclerView(recyclerView);

        lessonList = Setter.getAllLessons();
        lessonAdapter = new LessonAdapter(this, lessonList);

        recyclerView.setAdapter(lessonAdapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addLesson();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();

        lessonList = Setter.getAllLessons();
        lessonAdapter = new LessonAdapter(this, lessonList);
        recyclerView.setAdapter(lessonAdapter);

    }

    private void addLesson() {
        Intent intent = new Intent(this, AddLesson.class);
        startActivity(intent);
    }
    private ItemTouchHelper.Callback createHelperCllback() {
        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                        int adapterPosition = viewHolder.getAdapterPosition();
                        createDeleteWarning(adapterPosition);


                    }
                };
        return simpleCallback;
    }
    private void createDeleteWarning(final int adapterPosition) {
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
        alertDlg.setMessage("Do you want to delete? ");
        alertDlg.setCancelable(false);

        alertDlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteItem(adapterPosition);
            }
        });

        alertDlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                recyclerView.getAdapter().notifyDataSetChanged();

            }
        });

        alertDlg.create().show();
    }
    private void deleteItem(int adapterPosition) {
        Lesson lesson = lessonList.get(adapterPosition);
        int id_lesson = lesson.getId();
        Setter.deleteLessonSetter(id_lesson);
        lessonList.remove(adapterPosition);
        lessonAdapter.notifyItemRemoved(adapterPosition);
        onResume();
    }

}
