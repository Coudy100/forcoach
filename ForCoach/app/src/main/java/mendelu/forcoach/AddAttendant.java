package mendelu.forcoach;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import mendelu.forcoach.mendelu.forcoach.Setter;

public class AddAttendant extends AppCompatActivity {
    EditText editName;
    EditText editSurmame;
    EditText editNickname;
    EditText editPhone;
    EditText editEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_player);
        this.setTitle(getString(R.string.add_attendant));

        editName = (EditText) findViewById(R.id.attendant_name);
        editSurmame = (EditText) findViewById(R.id.attendant_surname);
        editNickname = (EditText) findViewById(R.id.attendant_nickname);
        editPhone = (EditText) findViewById(R.id.attendant_phone);
        editEmail = (EditText) findViewById(R.id.attendant_email);

        Button btnAdd = (Button) findViewById(R.id.btn_addAttendant);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAttendant();
            }
        });

    }

    public void addAttendant() {
        Log.d("HLASKA", "vytvarim Attendanta");
        String name = editName.getText().toString().trim();
        String surname = editSurmame.getText().toString().trim();
        String nickname = editNickname.getText().toString().trim();
        String phone = editPhone.getText().toString().trim();
        String email = editEmail.getText().toString().trim();

        Setter.addAttendantSetter(name, surname, nickname, phone, email);
        finish();
        //Intent intent = new Intent(this, ActivityAttendant.class);
        //intent.putExtra("FROM", "AddAttendant");
        //startActivity(intent);
    }
}
