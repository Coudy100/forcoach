package mendelu.forcoach;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.List;

import mendelu.forcoach.mendelu.forcoach.Setter;
import mendelu.forcoach.mendelu.forcoach.adapters.AttendantAdapter;
import mendelu.forcoach.mendelu.forcoach.adapters.AttendantAdapterInAddGroup;
import mendelu.forcoach.mendelu.forcoach.adapters.AttendantAdapterInDetailLesson;
import mendelu.forcoach.mendelu.forcoach.adapters.GroupAdapter;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendance;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Attendant;
import mendelu.forcoach.mendelu.forcoach.repositories.entities.Lesson;

public class DetailLesson extends AppCompatActivity {
    private RecyclerView recyclerView;
    private AttendantAdapterInDetailLesson attendantAdapter;
    private List<Attendant> attendantList;
    private List<Attendance> attendanceList;

    Lesson lesson;

    TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_lesson);

        int id = getIntent().getIntExtra("id",0);

        lesson = Setter.getLessonById(id);
        attendantList = new ArrayList<>();

        name = (TextView) findViewById(R.id.textView_lessonDetailName);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_detailLesson);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        name.setText(lesson.getName());
        this.setTitle("Lesson Detail");


        List<Attendance> attendanceList = Setter.getAttendancesByLessonId(lesson.getId());

        for (Attendance attendance: attendanceList) {
            Attendant attendant = Setter.getAttendantById(attendance.getAttendant().getId());
            attendantList.add(attendant);
        }

        attendantAdapter = new AttendantAdapterInDetailLesson(this, attendanceList);
        recyclerView.setAdapter(attendantAdapter);

    }
}
